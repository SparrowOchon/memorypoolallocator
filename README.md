# Memory Pool
The following servers as a general purpose implementation of a MemoryPoolAllocator or a Bump-Allocator for C++.
Its been designed specifically for embedded systems as such uses Malloc and no Templating.

## Usage
The following was built as a boilerplate to be used in other projects. The file breakdown is as follows:
 -  **MemoryPool**: Here lies the CORE of the Allocator NOTE: Comment out the Deallocator if your OS Cleans up process memory.
 -  **Memory**: Template of a CHUNK Node to be used in a linked list.
 -  **main.cpp**: Example Usage as seen in TestClass operator overloading and Default CHUNK size allocation is all thats needed.
 
 **NOTE**: The following `MemoryPool` Class contains a deconstructor since the class should be declared as static we can remove the Deconstructor and let the OS clean up after us IF WE ARE ON AN OS THAT HAS the ability to clean up.
 
## Installation:
No additional Libraries required.


# LICENSE:
```
	Show don't Sell License Version 1
https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md
	Copyright (c) 2020, Network Silence
		All rights reserved.
```
