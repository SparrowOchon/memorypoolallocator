/**

Date:				Feb 28, 2020
Software Designer		Network Silence
Purpose:			Memory Pool specifically made for Embedded Systems

Description:			The following program is a POC of a MemoryPool Allocator that uses the
 Heap through Malloc to allocate and assign memory chunks. It is designed specifically for
 Embedded Systems as such its been kept as simple as possible and avoids the use of templates.

 Classes:
    MemoryPool: Here lies the CORE of the Allocator NOTE: Comment out the Deallocator if your OS Cleans up process memory.
    Memory: Template of a CHUNK Node to be used in a linked list
    main.cpp: Example Usage as seen in TestClass operator overloading and Default CHUNK size allocation is all thats needed


*/

#include "MemoryPool.hpp"
#include <iostream>

/**
 * @brief Example Struct using the Allocator note any data inside is fine as long as the Allocator is static and the new and delete are overloaded.
 */
struct TestClass {
	static MemoryPoolAllocator allocator;

	// data, 16 bytes:
	uint64_t data[2];

	static void *operator new(size_t size)
	{
		return allocator.allocate(size);
	}

	static void operator delete(void *chunk_pointer, size_t size)
	{
		return allocator.deallocate(chunk_pointer);
	}
};

// Declare a CHUNK limit per BLOCK for our Test Class
MemoryPoolAllocator TestClass::allocator{ 8 };

int main(int argc, char const *argv[])
{
	constexpr int array_size = 13;

	TestClass *test_array[array_size]; // Array of Structs to test with

	std::cout << "size(TestClass): " << sizeof(TestClass) << std::endl << std::endl;

	std::cout
		<< "----------------------------------------------------------------------------------------------------------------------"
		<< std::endl;
	std::cout << "Test allocated:  " << array_size << " " << std::endl;

	for (int i = 0; i < array_size; ++i) {
		test_array[i] = new TestClass(); // Use Our Allocator to initialize into BLOCKS of 8 CHUNKS
		std::cout << "new [" << i << "] = " << test_array[i] << std::endl;
	}

	std::cout
		<< "----------------------------------------------------------------------------------------------------------------------"
		<< std::endl;

	for (int i = array_size - 1; i >= 0; --i) {
		std::cout << "delete [" << i << "] = " << test_array[i]
			  << std::endl; // Use Our deallocator to overwrite CHUNKS. NOTE these are NOT FREED
		delete test_array[i];
	}
	std::cout
		<< "----------------------------------------------------------------------------------------------------------------------"
		<< std::endl;

	return 0;
}
