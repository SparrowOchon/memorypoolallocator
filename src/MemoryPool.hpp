#ifndef MEMORYPOOL_MEMORYPOOLALLOCATOR_HPP
#define MEMORYPOOL_MEMORYPOOLALLOCATOR_HPP

#include "Memory.h"
#include <cstdio>

class MemoryPoolAllocator {
    public:
	/**
     	* @brief Contructor Initialize the MemoryPoolAllocator
     	* @param initial_chunk_count The amount of CHUNKS
     	*/
	MemoryPoolAllocator(const size_t initial_chunk_count) : chunks_per_block(initial_chunk_count)
	{
	}

	/**
     	* @brief Deallocator to free malloc'd block NOTE this will only get
	* EXECUTED ON EXIT(THUS AVOID IF YOUR OS FREEs MEMORY FOR YOU)
     	*/
	~MemoryPoolAllocator();

	/**
 	* @brief Get free CHUNK from BLOCK. Make new BLOCK if no room
	 * @param size size of the data that will fit into a CHUNK
	 * @return Return first free CHUNK in BLOCK
 	*/
	void *allocate(size_t size);

	/**
     	* @brief Set the CHUNK as a Free CHUNK
     	* @param chunk CHUNK we want to overwrite.
     	*/
	void deallocate(void *memory_start_ptr);

    private:
	/**
     	* @brief User defined CHUNKS per BLOCK.
     	*/
	size_t chunks_per_block;

	/**
     	* @brief Numbers of BLOCKS created
     	*/
	size_t blocks_allocated = 0;
	/**
     	* @brief Linked list of Memory Allocated by OS
     	*/
	Memory_Chunk *memory_allocated = nullptr;

	/**
     	* @brief  Allocate new memory BLOCK from OS
     	* @param chunk_size The size of Each Individual CHUNK in the BLOCK
     	* @return Pointer to the first CHUNK at the beginning of the BLOCK
     	*/
	Memory_Chunk *allocate_block(size_t chunk_size);
};

#endif //MEMORYPOOL_MEMORYPOOLALLOCATOR_HPP
