#include <malloc.h>
#include <iostream>
#include "MemoryPool.hpp"

/**
 * @brief Free Malloc'd memory on Class Cleanup.
 * NOTE: COMMENT OUT THE DECONSTRUCTOR IF ON A OS THAT WILL AUTO CLEAN UP.
 */
MemoryPoolAllocator::~MemoryPoolAllocator()
{
	Memory_Chunk *first_chunk;
	size_t cur_chunk = 1;
	size_t cur_block = 1;

	// O(C* B-1) => Chunk * Block Count - 1 Thus on singular blocks it will be O(1)
	while (memory_allocated != nullptr) {
		if (cur_chunk == 1) {
			first_chunk = memory_allocated; // Store start of block
		}
		if (cur_block != blocks_allocated) { // Only increment chunks if the current block is not the last
			memory_allocated = memory_allocated->next;
			if (cur_chunk !=
			    chunks_per_block) { // We want to increment to the END of the block first before deleting to avoid undefined behavior.
				++cur_chunk;
				continue;
			}
		}
		free(first_chunk);
		first_chunk = nullptr;
		if (cur_block == blocks_allocated) { // Break if we Deleted All the blocks Allocated
			break;
		}
		++cur_block;
		cur_chunk = 1;
	}
}

/**
  * @brief  Allocate new memory BLOCK from OS
  * @param chunk_size The Size of Each Individual CHUNK in the BLOCK
  * @return Pointer to the first CHUNK at the beginning of the BLOCK
  */
Memory_Chunk *MemoryPoolAllocator::allocate_block(const size_t chunk_size)
{
	std::cout << std::endl << "New block Creation: " << chunks_per_block << std::endl << std::endl;
	size_t block_size = chunks_per_block * chunk_size;

	Memory_Chunk *block_start = reinterpret_cast<Memory_Chunk *>(malloc(block_size));

	Memory_Chunk *chunk = block_start;
	for (int i = 0; i < chunks_per_block - 1; ++i) { // Link CHUNKS together in the BLOCK
		chunk->next = reinterpret_cast<Memory_Chunk *>(reinterpret_cast<char *>(chunk) + chunk_size);
		chunk = chunk->next;
	}
	chunk->next = nullptr;

	return block_start;
}

/**
  * @brief Get free CHUNK from BLOCK. Make new BLOCK if no room
  * @param size size of the data that will fit into a CHUNK
  * @return Return first free CHUNK in BLOCK
  */
void *MemoryPoolAllocator::allocate(size_t size)
{
	if (memory_allocated == nullptr) {
		memory_allocated = allocate_block(size);
		++blocks_allocated;
	}
	Memory_Chunk *empty_chunk = memory_allocated;
	memory_allocated = memory_allocated->next; // If none left memory_allocated will be nullptr
	return empty_chunk;
}

/**
  * @brief Set the CHUNK as a Free CHUNK
  * @param chunk CHUNK we want to overwrite.
  */
void MemoryPoolAllocator::deallocate(void *chunk)
{
	reinterpret_cast<Memory_Chunk *>(chunk)->next = memory_allocated;
	memory_allocated = reinterpret_cast<Memory_Chunk *>(chunk);
}
