#ifndef MEMORYPOOL_MEMORY_H
#define MEMORYPOOL_MEMORY_H

/**
 * @brief A Class to represent each MEMORY CHUNK in the BLOCK
 */
class Memory_Chunk {
    public:
	Memory_Chunk *next;
};

#endif //MEMORYPOOL_MEMORY_H
